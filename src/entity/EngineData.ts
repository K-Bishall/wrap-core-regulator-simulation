import { FlowRate } from 'entity/FlowRate.js';

export interface EngineData {
    intermix: number;
    flowRate: FlowRate;
}