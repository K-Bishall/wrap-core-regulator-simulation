export enum FlowRate {
    OPTIMAL = 'OPTIMAL',
    HIGH = 'HIGH',
    LOW = 'LOW'
}