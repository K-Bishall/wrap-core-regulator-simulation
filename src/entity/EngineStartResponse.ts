export interface EngineStartResponse {
    status: string;
    message: string;
    authorizationCode: string;
}