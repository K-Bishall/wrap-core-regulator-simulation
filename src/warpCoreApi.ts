import axios, { AxiosResponse } from 'axios';
import { EngineStartRequest } from './entity/EngineStartRequest.js';
import { EngineData } from './entity/EngineData.js';
import { EngineStartResponse } from './entity/EngineStartResponse.js';
import { AdjustmentRequest } from './entity/AdjustmentRequest.js';

const apiBaseUrl = 'https://warp-regulator-bd7q33crqa-lz.a.run.app/api';
let authorizationCode = '';

export async function startEngine(name: string, email: string): Promise<void> {
    const startRequest: EngineStartRequest = {name, email};
    const response = await axios.post(`${apiBaseUrl}/start`, startRequest);

    const data: EngineStartResponse = response.data;

    authorizationCode = data.authorizationCode;
}

export async function getEngineStatus(): Promise<EngineData> {
    const response: AxiosResponse<EngineData> = await axios.get(`${apiBaseUrl}/status?authorizationCode=${authorizationCode}`);
    return response.data;
}

export async function adjustMatter(adjustment: number): Promise<void> {
    const adjustmentRequest: AdjustmentRequest = {authorizationCode, value: adjustment};

    await axios.post(`${apiBaseUrl}/adjust/matter`, adjustmentRequest);
}

export async function adjustAntiMatter(adjustment: number): Promise<void> {
    const adjustmentRequest: AdjustmentRequest = {authorizationCode, value: adjustment};

    await axios.post(`${apiBaseUrl}/adjust/antimatter`, adjustmentRequest);
}
