export interface EngineStartRequest {
    name: string;
    email: string;
}