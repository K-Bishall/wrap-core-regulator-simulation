export interface AdjustmentRequest {
    authorizationCode: string;
    value: number;
}