# Wrap Core Regulator Simulation

### Problem Description
https://warp-regulator-bd7q33crqa-lz.a.run.app/

The project is implemented in Node.js with Typescript.

Install the dependencies:
`npm install`

Run the code:
`npm start`