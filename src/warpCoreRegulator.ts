import { AxiosError } from 'axios';
import {
    adjustAntiMatter,
    adjustMatter,
    getEngineStatus,
    startEngine,
} from './warpCoreApi.js';
import { EngineData } from './entity/EngineData.js';
import { FlowRate } from './entity/FlowRate.js';


const name = 'Captain Name';
const email = 'captail@warpcore.com';

const optimalLevel = 0.5;
const tolerance = 0.02;
const lowerLimit = optimalLevel - tolerance;
const upperLimit = optimalLevel + tolerance;


function getMatterAntimatterAmount(intermix: number, flowRate: FlowRate): { matter: number; antimatter: number } {
    const isWithinLimit = (intermix >= lowerLimit) && (intermix <= upperLimit);
    const intermixLevel = isWithinLimit ? 'good' : (intermix < lowerLimit) ? 'low' : 'high';

    const adjustmentForFlowRate = {
        [FlowRate.OPTIMAL]: {
            'low': {matter: 0.2, antimatter: 0},
            'good': {matter: 0, antimatter: 0},
            'high': {matter: 0, antimatter: 0.2},
        },
        [FlowRate.HIGH]: {
            'low': {matter: 0.2, antimatter: -0.2},
            'good': {matter: -0.2, antimatter: -0.2},
            'high': {matter: -0.2, antimatter: 0.2},
        },
        [FlowRate.LOW]: {
            'low': {matter: 0.2, antimatter: 0},
            'good': {matter: 0.2, antimatter: 0.2},
            'high': {matter: 0, antimatter: 0.2},
        },
    };

    return adjustmentForFlowRate[flowRate][intermixLevel];
}

async function adjustMatterAntimatter(engineData: EngineData) {
    const {intermix, flowRate} = engineData;

    const {matter, antimatter} = getMatterAntimatterAmount(intermix, flowRate);

    if (matter !== 0) {
        console.log(`Adding matter: ${matter}`);
        await adjustMatter(matter);
    }

    if (antimatter !== 0) {
        console.log(`Adding anti-matter: ${antimatter}`);
        await adjustAntiMatter(antimatter);
    }
}

async function runEngine() {
    try {
        await startEngine(name, email);
        console.log('Engine started');

        let runTime = 60; //seconds

        while (runTime > 0) {
            const engineData = await getEngineStatus();
            console.log(`Engine status: Intermix: ${engineData.intermix.toFixed(2)}, Flow rate: ${engineData.flowRate}`);

            await adjustMatterAntimatter(engineData);

            // wait for 1 second
            await new Promise((resolve) => setTimeout(resolve, 1000));
            runTime -= 1;
        }

        console.log('Engine ran successfully for at least 1 minute!');
    } catch (err) {
        const error = err as AxiosError;
        console.error('An error occurred:', error.response?.data || error.message);
    }
}

export default runEngine;